<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('export','Frontend@excel_test');

Route::post('user-registration','Frontend@user_registration');

Route::get('logout','Frontend@logout');

Route::get('user-registration-success',function(){
       return view('frontend/registration-success'); 
});

Route::get('confirm-user','Frontend@confirm_user');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'admin','middleware' => ['auth','role:2']], function () {
    Route::get('/','Admin@index');
    Route::get('profile/{user_id}','Admin@profile');
    Route::post('update-profile','Admin@update_profile');
    Route::get('user_list','Admin@user_list');
    
});


