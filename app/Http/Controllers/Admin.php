<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\loan_amount;
use App\proceed;
use App\loan_purpose;


use Illuminate\Support\Facades\Input;

class Admin extends Controller
{
    public function index() {
        
       

        return view('admin.index',$data);
        
    }
    
    public function blank() {
        
        return view('admin.blank');
        
    }
    
    public function profile($user_id) {  
        
        $data['users'] = User::find($user_id)->toArray();

        return view('admin.profile',$data);
        
    }
    
    public function update_profile(Request $request) {
        
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            
        ]);
        
        $update = array();
        $update['name'] = $request->name;
        $update['phone'] = $request->phone;
        if ($request->password != '') {
            $update['password'] = bcrypt($request->password);
        }        
        
        
        if (Input::hasFile('profile_picture')) {
            $file = Input::file('profile_picture');
            $file = $file->move(public_path() . '/assets/pics/profile_picture', rand(10000, 99999) . time() . '.' . $file->getClientOriginalExtension());
            $name = explode('/', $file->getRealPath());
            $update['profile_picture'] = end($name);
            
        }
        
        User::where('id','=',$request->user()->id)
              ->update($update);
        
        return redirect()->back()->with('message','Profile is successfully updated');
        
    }

    public function User_list(Request $request)
   {
       // $id=$request->user()->id;
       $data1['data1'] = user::select('*')
                ->where('role_id',1)
                ->get()
            ->toArray();

       $data['data'] =loan_amount::        
                 join('proceeds','loan_amounts.user_id','=','proceeds.user_id')
                 ->join('loan_purposes','proceeds.user_id','=','loan_purposes.user_id')
                 
                ->get()
                ->toArray();
        //print_r($data);
  return view('admin/user_list',$data1)->with($data);
   
   }
    
    
}
