<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Session;

class Frontend extends Controller
{
    
    public function user_registration(Request $request) {
        
        
        
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'            
            
        ]);
        
        
        $user                      = new User();
        $user->name                = $request->name;
        $user->email               = $request->email;
        $user->password            = bcrypt($request->password);        
        
        $confirmation_code         = time().rand(1, 10000);
        $user->confirmation_code   = $confirmation_code;
        $user->activation_status   = 0;
        $user->save();
        
        
        
        // Send the email confirmation email to user to verify
        
        $email_data = array(
            'email'             => $request->email,
            'confirmation_code' => $confirmation_code
        );
        
        
        Mail::send('frontend.email.register', $email_data, function($message) use ($request) {     

        
        $message->replyTo($request->email,$request->first_name.' '.$request->last_name);
        $message->subject('Registration Successful');
        $message->to($request->email);
        });
        
        return redirect('user-registration-success')->with('message','Thank you, Check your mail to activate your account.');
        
        
        
    }
    
    public function confirm_user(Request $request) {
        
        $this->validate($request,[
           'email' => 'required|email',
           'confirmation_code' => 'required|numeric'
        ]);
        
        $count = User::where('email','=',$request->email)
                 ->where('confirmation_code','=',$request->confirmation_code)
                 ->count();
        
         if ($count > 0) {
             
             $user = User::where('email','=',$request->email)
                     ->where('confirmation_code','=',$request->confirmation_code);
             
             $update = array(
                 'activation_status' => '1',
                 
             );
             $user->update($update);
             
                 
             
             
             return redirect('user-registration-success')->with('message','Thank you, you are successfuly registered.');
             
         }
        
    }
    
    public function logout() {
        
        \Illuminate\Support\Facades\Auth::logout();
        
        return redirect('/');
        
    }
	
	public function excel_test(){
		Excel::create('swatijoshi', function($excel) {

			$excel->sheet('iloveyou', function($sheet) {

				$sheet->fromArray(array(
					array('data1', 'data2'),
					array('data3', 'data4')
				));

			});

		})->export('xls');
	}
	
	
}
