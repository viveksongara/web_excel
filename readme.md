# Laravel Starter Kit, with admin and profile page

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

## Follow these steps ##

1) navigation to project direcotry, type composer update  
2) change the name of the database in .env file  
3) php migrate  
4) php optimize  
5) go to storage->framework directory, clean all the files in log, cache, session , view with hash values and delete laravel.log file from logs folder  

Thats it, Enjoy !