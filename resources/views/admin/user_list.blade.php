@extends('layouts.admin.master')

@section('mainContent')

<div class="col-md-12 col-sm-12">
<div class="col-sm-12 col-md-12" >

<table border="1" class="table table-bordered  ">
  <thead>
    <tr>
        <th>Name</th>
        <th>Business</th>
        <th>Address</th>
        <th>Country</th>
        <th>Referal</th>
        <th>JobCreation</th>
        
    </tr>
  </thead>
  <tbody>
      @foreach($data1 as $datas)
    <tr>
      <td>{{$datas['name']}}</td>
      <td>{{$datas['business_name']}}</td>
      <td>{{$datas['business_address']}}</td>
      <td>{{$datas['country']}}</td>
      <td>{{$datas['referal_source']}}</td>
      <td>{{$datas['job_creation']}}</td>
    

    </tr>
    <tr>
      @endforeach
    
  </tbody>
</table> 
</div>

<div class="col-sm-12 col-md-12" >
<hr>
<table border="1" class="table table-bordered  ">
  <thead>
    <tr>
        <th>Loan Purpose</th>
        <th>RBAC</th>
        <th>Borrower</th>
        <th>Total</th>
        <th>Loan Amount</th>
       <th>Interest Rate</th>
       <th>Interest  </th>
        <th>EMI</th>
       
    </tr>
  </thead>
  <tbody>
      @foreach($data as $datas)
    <tr>
      <td>{{$datas['Purpose']}}</td>
      <td>{{$datas['rbac']}}</td>
      <td>{{$datas['borrow']}}</td>
      <td>{{$datas['total']}}</td>
      <td>{{$datas['loan_amount']}}</td>
      <td>{{$datas['rate']}}% </td>
      <td><?php echo $rs= (($datas['loan_amount']*$datas['rate']*$datas['duration'])/100); ?></td>
      <td><?php echo $rs/12 ?></td>
      
    </tr>
    <tr>
      @endforeach
    
  </tbody>
</table> 


</div>
 
 


</div>
</div>

@stop