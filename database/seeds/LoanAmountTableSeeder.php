<?php

use Illuminate\Database\Seeder;

class LoanAmountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('loan_amounts')->insert([
            'user_id' => '1',
            'rate' => '6.25',
            'duration' =>'1',
            'loan_amount'=>'50000'
           
        ]);
           
        DB::table('loan_amounts')->insert([
            'user_id' => '2',
            'rate' => '6',
            'duration' =>'1',
            'loan_amount'=>'500000'
           
        ]);
        DB::table('loan_amounts')->insert([
            'user_id' => '3',
            'rate' => '6',
            'duration' =>'1',
            'loan_amount'=>'500000'
           
        ]);  
    }
}
